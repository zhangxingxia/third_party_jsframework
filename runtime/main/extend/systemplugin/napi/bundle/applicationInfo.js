/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const ApplicationInfoMock = {
    name: "[PC preview] unknown name",
    description: "[PC preview] unknown description",
    descriptionId: "[PC preview] unknown descriptionId",
    systemApp: "[PC preview] unknown systemApp",
    enabled: "[PC preview] unknown enabled",
    label: "[PC preview] unknown label",
    labelId: "[PC preview] unknown labelId",
    icon: "[PC preview] unknown icon",
    iconId: "[PC preview] unknown iconId",
    process: "[PC preview] unknown process",
    supportedModes: "[PC preview] unknown supportedModes",
    moduleSourceDirs: ["[PC preview] unknown moduleSourceDirs"],
    permissions: ["[PC preview] unknown permissions"],
    moduleInfos: [
        {
            moduleName: "[PC preview] unknown moduleName",
            moduleSourceDir: "[PC preview] unknown moduleSourceDir",
        }
    ],
    entryDir: "[PC preview] unknown entryDir",
    codePath: "[PC preview] unknown codePath",
    metaData: [{
        name: "[PC preview] unknown name",
        value: "[PC preview] unknown value",
        extra: "[PC preview] unknown extra",
    }],
    metadata: [{
        name: "[PC preview] unknown name",
        value: "[PC preview] unknown value",
        resource: "[PC preview] unknown resource",
    }],
    removable: "[PC preview] unknown removable",
    accessTokenId: "[PC preview] unknown accessTokenId",
    uid: "[PC preview] unknown uid",
    entityType: "[PC preview] unknown entityType",
    fingerprint: "[PC preview] unknown fingerprint",
    iconResource: {
        bundleName: "[PC preview] unknown bundleName",
        moduleName: "[PC preview] unknown moduleName",
        id: "[PC preview] unknown id"
    },
    labelResource: {
        bundleName: "[PC preview] unknown bundleName",
        moduleName: "[PC preview] unknown moduleName",
        id: "[PC preview] unknown id"
    },
    descriptionResource: {
        bundleName: "[PC preview] unknown bundleName",
        moduleName: "[PC preview] unknown moduleName",
        id: "[PC preview] unknown id"
    },
    appDistributionType: "[PC preview] unknown appDistributionType",
    appProvisionType: "[PC preview] unknown appProvisionType", 
}

export const WantMock = {
    deviceId: "[PC Preview] unknow deviceId",
    bundleName: "[PC Preview] unknow bundleName",
    abilityName: "[PC Preview] unknow abilityName",
    uri: "[PC Preview] unknow uri",
    type: "[PC Preview] unknow type",
    flag: "[PC Preview] unknow flag",
    action: "[PC Preview] unknow action",
    parameters: ["[PC Preview] unknow parameters"],
    entities: ["[PC Preview] unknow entities"],
    moduleName: "[PC Preview] unknow moduleName",
}

export const ShortcutInfoMock =  {
    id: "[PC preview] unknown id",
    bundleName: "[PC preview] unknown bundleName",
    hostAbility: "[PC preview] unknown hostAbility",
    icon: "[PC preview] unknown icon",
    iconId: "[PC preview] unknown iconId",
    label: "[PC preview] unknown label",
    labelId: "[PC preview] unknown labelId",
    disableMessage: "[PC preview] unknown disableMessage",
    wants: [
        {
            targetBundle: "[PC preview] unknown targetBundle",
            targetModule: "[PC preview] unknown targetModule",
            targetClass: "[PC preview] unknown targetClass",
        }],
    isStatic: "[PC preview] unknown isStatic",
    isHomeShortcut: "[PC preview] unknown isHomeShortcut",
    isEnabled: "[PC preview] unknown isEnabled",
    moduleName: "[PC preview] unknown moduleName",
}

export const ModuleUsageRecordMock = {
    bundleName: "[PC preview] unknown bundleName",
    appLabelId: "[PC preview] unknown appLabelId",
    name: "[PC preview] unknown name",
    labelId: "[PC preview] unknown labelId",
    descriptionId: "[PC preview] unknown descriptionId",
    abilityName: "[PC preview] unknown abilityName",
    abilityLabelId: "[PC preview] unknown abilityLabelId",
    abilityDescriptionId: "[PC preview] unknown abilityDescriptionId",
    abilityIconId: "[PC preview] unknown abilityIconId",
    launchedCount: "[PC preview] unknown launchedCount",
    lastLaunchTime: "[PC preview] unknown lastLaunchTime",
    isRemoved: "[PC preview] unknown isRemoved",
    installationFreeSupported: "[PC preview] unknown installationFreeSupported",
}