/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const LauncherAbilityInfoMock = {
    applicationInfo: {
        packageName: "[PC preview] unknown packageName",
        description: "[PC preview] unknown description",
        descriptionId: "[PC preview] unknown descriptionId",
        systemApp: "[PC preview] unknown systemApp",
        enabled: "[PC preview] unknown enabled",
        className: "[PC preview] unknown className",
        name: "[PC preview] unknown name",
        labelId: "[PC preview] unknown labelId",
        label: "[PC preview] unknown label",
        icon: "[PC preview] unknown icon",
        iconId: "[PC preview] unknown iconId",
        process: "[PC preview] unknown process",
        supportedModes: "[PC preview] unknown supportedModes",
        moduleSourceDirs: ["[PC preview] unknown moduleSourceDirs"],
        permissions: ["[PC preview] unknown permissions"],
        sourceDir: "[PC preview] unknown sourceDir",
        moduleInfos: [{
            moduleName: "[PC preview] unknown moduleName",
            moduleSourceDir: "[PC preview] unknown moduleSourceDir",
        }],
        flags: "[PC preview] unknown flags",
        entryDir: "[PC preview] unknown entryDir",
        codePath: "[PC preview] unknown codePath",
        metaData: [{
            name: "[PC preview] unknown name",
            value: "[PC preview] unknown value",
            extra: "[PC preview] unknown extra",
        }],
        metadata: [{
            name: "[PC preview] unknown name",
            value: "[PC preview] unknown value",
            resource: "[PC preview] unknown resource",
        }],
        removable: "[PC preview] unknown removable",
        accessTokenId: "[PC preview] unknown accessTokenId",
        uid: "[PC preview] unknown uid",
        entityType: "[PC preview] unknown entityType",
        fingerprint: "[PC preview] unknown fingerprint",
        iconResource: {
            bundleName: "[PC preview] unknown bundleName",
            moduleName: "[PC preview] unknown moduleName",
            id: "[PC preview] unknown id",  
        },
        labelResource: {
            bundleName: "[PC preview] unknown bundleName",
            moduleName: "[PC preview] unknown moduleName",
            id: "[PC preview] unknown id",  
        },
        descriptionResource: {
            bundleName: "[PC preview] unknown bundleName",
            moduleName: "[PC preview] unknown moduleName",
            id: "[PC preview] unknown id",  
        },
        appDistributionType: "[PC preview] unknown appDistributionType",
        appProvisionType: "[PC preview] unknown appProvisionType",
    },
    elementName: {
        deviceId: "[PC preview] unknown deviceId",
        bundleName: "[PC preview] unknown bundleName",
        abilityName: "[PC preview] unknown abilityName",
        uri: "[PC preview] unknown uri",
        shortName: "[PC preview] unknown shortName",
        moduleName: "[PC preview] unknown moduleName",
    },
    labelId: "[PC preview] unknown labelId",
    iconId: "[PC preview] unknown iconId",
    userId: "[PC preview] unknown userId",
    installTime: "[PC preview] unknown installTime",
}