/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const BundleInfoMock = {
    name: "[PC preview] unknown name",
    type: "[PC preview] unknown type",
    appId: "[PC preview] unknown appId",
    uid: "[PC preview] unknown uid",
    installTime: "[PC preview] unknown installTime",
    updateTime: "[PC preview] unknown updateTime",
    appInfo: {
        packageName: "[PC preview] unknown packageName",
        description: "[PC preview] unknown description",
        descriptionId: "[PC preview] unknown descriptionId",
        systemApp: "[PC preview] unknown systemApp",
        enabled: "[PC preview] unknown enabled",
        className: "[PC preview] unknown className",
        name: "[PC preview] unknown name",
        labelId: "[PC preview] unknown labelId",
        label: "[PC preview] unknown label",
        icon: "[PC preview] unknown icon",
        iconId: "[PC preview] unknown iconId",
        process: "[PC preview] unknown process",
        supportedModes: "[PC preview] unknown supportedModes",
        moduleSourceDirs: ["[PC preview] unknown moduleSourceDirs"],
        permissions: ["[PC preview] unknown permissions"],
        sourceDir: "[PC preview] unknown sourceDir",
        moduleInfos: [{
            moduleName: "[PC preview] unknown moduleName",
            moduleSourceDir: "[PC preview] unknown moduleSourceDir",
        }],
        flags: "[PC preview] unknown flags",
        entryDir: "[PC preview] unknown entryDir",
        codePath: "[PC preview] unknown codePath",
        metaData: [{
            name: "[PC preview] unknown name",
            value: "[PC preview] unknown value",
            extra: "[PC preview] unknown extra",
        }],
        metadata: [{
            name: "[PC preview] unknown name",
            value: "[PC preview] unknown value",
            resource: "[PC preview] unknown resource",
        }],
        removable: "[PC preview] unknown removable",
        accessTokenId: "[PC preview] unknown accessTokenId",
        uid: "[PC preview] unknown uid",
        entityType: "[PC preview] unknown entityType",
        fingerprint: "[PC preview] unknown fingerprint",
        iconResource: {
            bundleName: "[PC preview] unknown bundleName",
            moduleName: "[PC preview] unknown moduleName",
            id: "[PC preview] unknown id",  
        },
        labelResource: {
            bundleName: "[PC preview] unknown bundleName",
            moduleName: "[PC preview] unknown moduleName",
            id: "[PC preview] unknown id",  
        },
        descriptionResource: {
            bundleName: "[PC preview] unknown bundleName",
            moduleName: "[PC preview] unknown moduleName",
            id: "[PC preview] unknown id",  
        },
        appDistributionType: "[PC preview] unknown appDistributionType",
        appProvisionType: "[PC preview] unknown appProvisionType",
        customizeData: {
            key: "[PC preview] unknown key",
            value: {
                CustomizeData: {
                    name: "[PC preview] unknown name",
                    value: "[PC preview] unknown value",
                }
            }
        }
    },
    abilityInfo: [
        {
            name: "[PC preview] unknown name",
            bundleName: "[PC preview] unknown bundleName",
            moduleName: "[PC preview] unknown moduleName",
            process: "[PC preview] unknown process",
            targetAbility: "[PC preview] unknown targetAbility",
            backgroundModes: "[PC preview] unknown backgroundModes",
            isVisible: "[PC preview] unknown isVisible",
            formEnabled: "[PC preview] unknown formEnabled",
            type: {
                UNKNOWN: 0,
                PAGE: 2,
                SERVICE: 3,
                DATA: 4,
            },
            subType: {
                UNSPECIFIED: 0,
                CA: 1,
            },
            orientation: {
                UNSPECIFIED: 0,
                LANDSCAPE: 2,
                PORTRAIT: 1,
                FOLLOW_RECENT: 2,
                LANDSCAPE_INVERTED: 4,
                PORTRAIT_INVERTED: 3,
                AUTO_ROTATION: 5,
                AUTO_ROTATION_LANDSCAPE: 7,
                AUTO_ROTATION_PORTRAIT: 6,
                AUTO_ROTATION_RESTRICTED: 8,
                AUTO_ROTATION_LANDSCAPE_RESTRICTED: 10,
                AUTO_ROTATION_PORTRAIT_RESTRICTED: 9,
                LOCKED: 11,
            },
            launchMode: {
                SINGLETON: 0,
                STANDARD: 1,
            },
            permissions: ['[PC Preview] unknow permissions'],
            deviceTypes: ['[PC Preview] unknow deviceTypes'],
            deviceCapabilities: ['[PC Preview] unknow deviceCapabilities'],
            readPermission: '[PC Preview] unknow readPermission',
            writePermission: '[PC Preview] unknow writePermission',
            appId: '[PC Preview] unknow appId',
            label: "[PC preview] unknown label",
            description: "[PC preview] unknown description",
            uid: '[PC Preview] unknow uid',
            installTime: '[PC Preview] unknow installTime',
            updateTime: '[PC Preview] unknow updateTime',
            descriptionId: '[PC Preview] unknow descriptionId',
            applicationInfo: {
                packageName: "[PC preview] unknown packageName",
                description: "[PC preview] unknown description",
                descriptionId: "[PC preview] unknown descriptionId",
                enabled: "[PC preview] unknown enabled",
                systemApp: "[PC preview] unknown systemApp",
                className: "[PC preview] unknown className",
                name: "[PC preview] unknown name",
                label: "[PC preview] unknown label",
                labelId: "[PC preview] unknown labelId",
                icon: "[PC preview] unknown icon",
                iconId: "[PC preview] unknown iconId",
                process: "[PC preview] unknown process",
                sourceDir: "[PC preview] unknown sourceDir",
                moduleSourceDirs: ["[PC preview] unknown moduleSourceDirs"],
                permissions: ["[PC preview] unknown permissions"],
                moduleInfos: [{
                    moduleName: "[PC preview] unknown moduleName",
                    moduleSourceDir: "[PC preview] unknown moduleSourceDir",
                }],
                entryDir: ["[PC preview] unknown entryDir"],
                codePath: ["[PC preview] unknown codePath"],
                metaData: [{
                    name: "[PC preview] unknown name",
                    value: "[PC preview] unknown value",
                    extra: "[PC preview] unknown extra",
                }],
                metadata: [{
                    name: "[PC preview] unknown name",
                    value: "[PC preview] unknown value",
                    resource: "[PC preview] unknown resource",
                }],
                removable: "[PC preview] unknown removable",
                accessTokenId: "[PC preview] unknown accessTokenId",
                uid: "[PC preview] unknown uid",
                entityType: "[PC preview] unknown entityType",
                fingerprint: "[PC preview] unknown fingerprint",
                iconResource: "[PC preview] unknown iconResource",
                labelResource: "[PC preview] unknown labelResource",
                descriptionResource: "[PC preview] unknown descriptionResource",
                appDistributionType: "[PC preview] unknown appDistributionType",
                appProvisionType: "[PC preview] unknown appProvisionType",
                supportedModes: "[PC preview] unknown supportedModes",
                flags: "[PC preview] unknown flags",
                customizeData: {
                    key: "[PC preview] unknown key",
                    value: {
                        CustomizeData: {
                            name: "[PC preview] unknown name",
                            value: "[PC preview] unknown value",
                        }
                    }
                }
            },
            metaData: [{
                name: "[PC preview] unknown name",
                value: "[PC preview] unknown value",
                extra: "[PC preview] unknown extra",
            }],
            metadata: [{
                name: "[PC preview] unknown name",
                value: "[PC preview] unknown value",
                resource: "[PC preview] unknown resource",
            }],
            enabled: "[PC preview] unknown enabled",
            supportWindowMode: [{
                FULL_SCREEN: 0,
                SPLIT: 1,
                FLOATING: 2,
            }],
            maxWindowRatio: "[PC preview] unknown maxWindowRatio",
            minWindowRatio: "[PC preview] unknown minWindowRatio",
            maxWindowWidth: "[PC preview] unknown maxWindowWidth",
            minWindowWidth: "[PC preview] unknown minWindowWidth",
            maxWindowHeight: "[PC preview] unknown maxWindowHeight",
            minWindowHeight: "[PC preview] unknown minWindowHeight",
            formEntity: "[PC preview] unknown formEntity",
            minFormHeight: "[PC preview] unknown minFormHeight",
            defaultFormHeight: "[PC preview] unknown defaultFormHeight",
            minFormWidth: "[PC preview] unknown minFormWidth",
            defaultFormWidth: "[PC preview] unknown defaultFormWidth",
            uri: "[PC preview] unknown uri",
            customizeData: {
                key: "[PC preview] unknown key",
                value: [
                    {
                        CustomizeData: {
                            name: "[PC preview] unknown name",
                            value: "[PC preview] unknown value",
                        },
                    },
                    {
                        CustomizeData: {
                            name: "[PC preview] unknown name",
                            value: "[PC preview] unknown value",
                        },
                    }
                ]
            },
        }
    ],
    reqPermissions: ["[PC preview] unknown repermission"],
    reqPermissionDetails: [
        {
            name: "[PC preview] unknown name",
            reason: "[PC preview] unknown reason",
            reasonId: "[PC preview] unknown reasonId",
            usedScene: {
                abilities: ["[PC preview] unknown ability"],
                when: "[PC preview] unknown when",
            }
        }
    ],
    vendor: "[PC preview] unknown vendor",
    versionCode: "[PC preview] unknown versionCode",
    versionName: "[PC preview] unknown versionName",
    compatibleVersion: "[PC preview] unknown compatibleVersion",
    targetVersion: "[PC preview] unknown targetVersion",
    isCompressNativeLibs: "[PC preview] unknown isCompressNativeLibs",
    hapModuleInfos: [{
        name: "[PC preview] unknown name",
        description: "[PC preview] unknown description",
        descriptionId: "[PC preview] unknown descriptionId",
        icon: "[PC preview] unknown icon",
        label: "[PC preview] unknown label",
        labelId: "[PC preview] unknown labelId",
        iconId: "[PC preview] unknown iconId",
        backgroundImg: "[PC preview] unknown backgroundImg",
        supportedModes: "[PC preview] unknown supportedModes",
        reqCapabilities: ["[PC preview] unknown reqCapabilities"],
        deviceTypes: ["[PC preview] unknown deviceTypes"],
        abilityInfo: [
            {
                name: "[PC preview] unknown name",
                bundleName: "[PC preview] unknown bundleName",
                moduleName: "[PC preview] unknown moduleName",
                process: "[PC preview] unknown process",
                targetAbility: "[PC preview] unknown targetAbility",
                backgroundModes: "[PC preview] unknown backgroundModes",
                isVisible: "[PC preview] unknown isVisible",
                formEnabled: "[PC preview] unknown formEnabled",
                type: {
                    UNKNOWN: 0,
                    PAGE: 2,
                    SERVICE: 3,
                    DATA: 4,
                },
                subType: {
                    UNSPECIFIED: 0,
                    CA: 1,
                },
                orientation: {
                    UNSPECIFIED: 0,
                    LANDSCAPE: 2,
                    PORTRAIT: 1,
                    FOLLOW_RECENT: 2,
                    LANDSCAPE_INVERTED: 4,
                    PORTRAIT_INVERTED: 3,
                    AUTO_ROTATION: 5,
                    AUTO_ROTATION_LANDSCAPE: 7,
                    AUTO_ROTATION_PORTRAIT: 6,
                    AUTO_ROTATION_RESTRICTED: 8,
                    AUTO_ROTATION_LANDSCAPE_RESTRICTED: 10,
                    AUTO_ROTATION_PORTRAIT_RESTRICTED: 9,
                    LOCKED: 11,
                },
                launchMode: {
                    SINGLETON: 0,
                    STANDARD: 1,
                },
                permissions: ['[PC Preview] unknow permissions'],
                deviceTypes: ['[PC Preview] unknow deviceTypes'],
                deviceCapabilities: ['[PC Preview] unknow deviceCapabilities'],
                readPermission: '[PC Preview] unknow readPermission',
                writePermission: '[PC Preview] unknow writePermission',
                appId: '[PC Preview] unknow appId',
                label: "[PC preview] unknown label",
                description: "[PC preview] unknown description",
                uid: '[PC Preview] unknow uid',
                installTime: '[PC Preview] unknow installTime',
                updateTime: '[PC Preview] unknow updateTime',
                descriptionId: '[PC Preview] unknow descriptionId',
                applicationInfo: {
                    packageName: "[PC preview] unknown packageName",
                    description: "[PC preview] unknown description",
                    descriptionId: "[PC preview] unknown descriptionId",
                    enabled: "[PC preview] unknown enabled",
                    systemApp: "[PC preview] unknown systemApp",
                    className: "[PC preview] unknown className",
                    name: "[PC preview] unknown name",
                    label: "[PC preview] unknown label",
                    labelId: "[PC preview] unknown labelId",
                    icon: "[PC preview] unknown icon",
                    iconId: "[PC preview] unknown iconId",
                    process: "[PC preview] unknown process",
                    sourceDir: "[PC preview] unknown sourceDir",
                    moduleSourceDirs: ["[PC preview] unknown moduleSourceDirs"],
                    permissions: ["[PC preview] unknown permissions"],
                    moduleInfos: [{
                        moduleName: "[PC preview] unknown moduleName",
                        moduleSourceDir: "[PC preview] unknown moduleSourceDir",
                    }],
                    entryDir: ["[PC preview] unknown entryDir"],
                    codePath: ["[PC preview] unknown codePath"],
                    metaData: [{
                        name: "[PC preview] unknown name",
                        value: "[PC preview] unknown value",
                        extra: "[PC preview] unknown extra",
                    }],
                    metadata: [{
                        name: "[PC preview] unknown name",
                        value: "[PC preview] unknown value",
                        resource: "[PC preview] unknown resource",
                    }],
                    removable: "[PC preview] unknown removable",
                    accessTokenId: "[PC preview] unknown accessTokenId",
                    uid: "[PC preview] unknown uid",
                    entityType: "[PC preview] unknown entityType",
                    fingerprint: "[PC preview] unknown fingerprint",
                    iconResource: "[PC preview] unknown iconResource",
                    labelResource: "[PC preview] unknown labelResource",
                    descriptionResource: "[PC preview] unknown descriptionResource",
                    appDistributionType: "[PC preview] unknown appDistributionType",
                    appProvisionType: "[PC preview] unknown appProvisionType",
                    supportedModes: "[PC preview] unknown supportedModes",
                    flags: "[PC preview] unknown flags",
                    customizeData: {
                        key: "[PC preview] unknown key",
                        value: {
                            CustomizeData: {
                                name: "[PC preview] unknown name",
                                value: "[PC preview] unknown value",
                            }
                        }
                    }
                },
                metaData: [{
                    name: "[PC preview] unknown name",
                    value: "[PC preview] unknown value",
                    extra: "[PC preview] unknown extra",
                }],
                metadata: [{
                    name: "[PC preview] unknown name",
                    value: "[PC preview] unknown value",
                    resource: "[PC preview] unknown resource",
                }],
                enabled: "[PC preview] unknown enabled",
                supportWindowMode: [{
                    FULL_SCREEN: 0,
                    SPLIT: 1,
                    FLOATING: 2,
                }],
                maxWindowRatio: "[PC preview] unknown maxWindowRatio",
                minWindowRatio: "[PC preview] unknown minWindowRatio",
                maxWindowWidth: "[PC preview] unknown maxWindowWidth",
                minWindowWidth: "[PC preview] unknown minWindowWidth",
                maxWindowHeight: "[PC preview] unknown maxWindowHeight",
                minWindowHeight: "[PC preview] unknown minWindowHeight",
                formEntity: "[PC preview] unknown formEntity",
                minFormHeight: "[PC preview] unknown minFormHeight",
                defaultFormHeight: "[PC preview] unknown defaultFormHeight",
                minFormWidth: "[PC preview] unknown minFormWidth",
                defaultFormWidth: "[PC preview] unknown defaultFormWidth",
                uri: "[PC preview] unknown uri",
                customizeData: {
                    key: "[PC preview] unknown key",
                    value: [
                        {
                            CustomizeData: {
                                name: "[PC preview] unknown name",
                                value: "[PC preview] unknown value",
                            },
                        },
                        {
                            CustomizeData: {
                                name: "[PC preview] unknown name",
                                value: "[PC preview] unknown value",
                            },
                        }
                    ]
                },
            }
        ],
        moduleName: "[PC preview] unknown moduleName",
        mainAbilityName: "[PC preview] unknown mainAbilityName",
        installationFree: "[PC preview] unknown installationFree",
        mainElementName: "[PC preview] unknown mainElementName",
        extensionAbilityInfo: [
            {
                bundleName: "[PC preview] unknown bundleName",
                moduleName: "[PC preview] unknown moduleName",
                name: "[PC preview] unknown name",
                labelId: "[PC preview] unknown labelId",
                descriptionId: "[PC preview] unknown descriptionId",
                iconId: "[PC preview] unknown iconId",
                isVisible: "[PC preview] unknown isVisible",
                extensionAbilityType: {
                    FORM: 0,
                    WORK_SCHEDULER: 1,
                    INPUT_METHOD: 2,
                    SERVICE: 3,
                    ACCESSIBILITY: 4,
                    DATA_SHARE: 5,
                    FILE_SHARE: 6,
                    STATIC_SUBSCRIBER: 7,
                    WALLPAPER: 8,
                    BACKUP: 9,
                    WINDOW: 10,
                    ENTERPRISE_ADMIN: 11,
                    UNSPECIFIED: 20,
                },
                metadata: [{
                    name: "[PC preview] unknown name",
                    value: "[PC preview] unknown value",
                    resource: "[PC preview] unknown resource",
                }],
                hashValue: "[PC preview] unknown hashValue",
            }
        ],
        metadata: "[PC preview] unknown mainElementName",
        hashValue: "[PC preview] unknown hashValue",
    }],
    hapModuleInfo: [
        {
            name: "string",
            description: "string",
            descriptionId: 1,
            icon: "string",
            label: "string",
            labelId: 2,
            iconId: 3,
            backgroundImg: "string",
            supportedModes: 4,
            reqCapabilities: ["1", "2"],
            deviceTypes: ["1", "2"],
            abilityInfo: [
                {
                    bundleName: "string",
                    name: "string",
                    label: "string",
                    description: "string",
                    icon: "string",
                    labelId: 1,
                    descriptionId: 2,
                    iconId: 3,
                    moduleName: "string",
                    process: "string",
                    targetAbility: "string",
                    backgroundModes: 4,
                    isVisible: true,
                    formEnabled: true,
                    type: "bundle.AbilityType",
                    subType: "bundle.AbilitySubType",
                    orientation: "bundle.DisplayOrientation",
                    launchMode: "bundle.LaunchMode",
                    permissions: ["1"],
                    deviceTypes: ["2"],
                    deviceCapabilities: ["3"],
                    readPermission: "string",
                    writePermission: "string",
                    applicationInfo: {
                        packageName: "[PC preview] unknown packageName",
                        className: "[PC preview] unknown className",
                        name: "[PC preview] unknown name",
                        labelId: 2,
                        iconId: 3,
                        sourceDir: "[PC preview] unknown sourceDir",
                        flags: 1,
                        customizeData: {
                            key: "[PC preview] unknown key",
                            value: {
                                CustomizeData: {
                                    name: "[PC preview] unknown name",
                                    value: "[PC preview] unknown value",
                                }
                            }
                        }
                    },
                    formEntity: 5,
                    minFormHeight: 6,
                    defaultFormHeight: 7,
                    minFormWidth: 8,
                    defaultFormWidth: 9,
                    uri: "string",
                    customizeData: {
                        key: "[PC preview] unknown key",
                        value: [
                            {
                                CustomizeData: {
                                    name: "[PC preview] unknown name",
                                    value: "[PC preview] unknown value",
                                },
                            },
                            {
                                CustomizeData: {
                                    name: "[PC preview] unknown name",
                                    value: "[PC preview] unknown value",
                                },
                            }
                        ]
                    },
                }
            ],
            moduleName: "string",
            mainAbilityName: "string",
            installationFree: true,
        }
    ],
    entryModuleName: "string",
    cpuAbi: "string",
    isSilentInstallation: "string",
    minCompatibleVersionCode: 7,
    entryInstallationFree: true,
    reqPermissionStates: ["[PC preview] unknown reqPermissionStates"],
    extensionAbilityInfo: [{
        bundleName: "[PC preview] unknown bundleName",
        moduleName: "[PC preview] unknown moduleName",
        name: "[PC preview] unknown name",
        labelId: "[PC preview] unknown labelId",
        descriptionId: "[PC preview] unknown descriptionId",
        iconId: "[PC preview] unknown iconId",
        isVisible: "[PC preview] unknown isVisible",
        extensionAbilityType: {
            FORM: 0,
            WORK_SCHEDULER: 1,
            INPUT_METHOD: 2,
            SERVICE: 3,
            ACCESSIBILITY: 4,
            DATA_SHARE: 5,
            FILE_SHARE: 6,
            STATIC_SUBSCRIBER: 7,
            WALLPAPER: 8,
            BACKUP: 9,
            WINDOW: 10,
            ENTERPRISE_ADMIN: 11,
            UNSPECIFIED: 20,
        },
        metadata: [{
            name: "[PC preview] unknown name",
            value: "[PC preview] unknown value",
            resource: "[PC preview] unknown resource",
        }],
        permissions: ["[PC preview] unknown hashValue"],
        applicationInfo: {
            packageName: "[PC preview] unknown packageName",
            description: "[PC preview] unknown description",
            descriptionId: "[PC preview] unknown descriptionId",
            enabled: "[PC preview] unknown enabled",
            systemApp: "[PC preview] unknown systemApp",
            className: "[PC preview] unknown className",
            name: "[PC preview] unknown name",
            label: "[PC preview] unknown label",
            labelId: "[PC preview] unknown labelId",
            icon: "[PC preview] unknown icon",
            iconId: "[PC preview] unknown iconId",
            process: "[PC preview] unknown process",
            moduleSourceDirs: ["[PC preview] unknown moduleSourceDirs"],
            permissions: ["[PC preview] unknown permissions"],
            moduleInfos: [{
                moduleName: "[PC preview] unknown moduleName",
                moduleSourceDir: "[PC preview] unknown moduleSourceDir",
            }],
            entryDir: ["[PC preview] unknown entryDir"],
            codePath: ["[PC preview] unknown codePath"],
            metaData: [{
                name: "[PC preview] unknown name",
                value: "[PC preview] unknown value",
                extra: "[PC preview] unknown extra",
            }],
            metadata: [{
                name: "[PC preview] unknown name",
                value: "[PC preview] unknown value",
                resource: "[PC preview] unknown resource",
            }],
            removable: "[PC preview] unknown removable",
            accessTokenId: "[PC preview] unknown accessTokenId",
            uid: "[PC preview] unknown uid",
            entityType: "[PC preview] unknown entityType",
            fingerprint: "[PC preview] unknown fingerprint",
            iconResource: "[PC preview] unknown iconResource",
            labelResource: "[PC preview] unknown labelResource",
            descriptionResource: "[PC preview] unknown descriptionResource",
            appDistributionType: "[PC preview] unknown appDistributionType",
            appProvisionType: "[PC preview] unknown appProvisionType",
            supportedModes: "[PC preview] unknown supportedModes",
        },
        metadata: [{
            name: "[PC preview] unknown name",
            value: "[PC preview] unknown value",
            resource: "[PC preview] unknown resource",
        }],
        enabled: "[PC preview] unknown enabled",
        readPermission: "[PC preview] unknown readPermission",
        writePermission: "[PC preview] unknown writePermission",
    }],
};

export const BundlePackInfo = {
    packages: [{
        deviceType: ["[PC preview] unknown deviceType"],
        name: "[PC preview] unknown name",
        moduleType: "[PC preview] unknown moduleType",
        deliveryWithInstall: "[PC preview] unknown deliveryWithInstall",
    }],
    summary: {
        app: {
            bundleName: "[PC preview] unknown bundleName",
            version: {
                minCompatibleVersionCode: "[PC preview] unknown minCompatibleVersionCode",
                name: "[PC preview] unknown name",
                code: "[PC preview] unknown code",
            },
        },
        modules: [
            {
                apiVersion: {
                    releaseType: "[PC preview] unknown releaseType",
                    compatible: "[PC preview] unknown compatible",
                    target: "[PC preview] unknown target",
                },
                deviceType: ["[PC preview] unknown deviceType"],
                distro: {
                    mainAbility: "[PC preview] unknown mainAbility",
                    deliveryWithInstall: "[PC preview] unknown deliveryWithInstall",
                    installationFree: "[PC preview] unknown installationFree",
                    moduleName: "[PC preview] unknown moduleName",
                    moduleType: "[PC preview] unknown moduleType",
                },
                abilities: [
                    {
                        name: "[PC preview] unknown name",
                        label: "[PC preview] unknown label",
                        visible: "[PC preview] unknown visible",
                        forms: [
                            {
                                name: "[PC preview] unknown name",
                                type: "[PC preview] unknown type",
                                updateEnabled: "[PC preview] unknown updateEnabled",
                                scheduledUpdateTime: "[PC preview] unknown scheduledUpdateTime",
                                updateDuration: "[PC preview] unknown updateDuration",
                                supportDimensions: ["[PC preview] unknown supportDimensions"],
                                defaultDimension: "[PC preview] unknown defaultDimension",
                            }
                        ],

                    }
                ],
                extensionAbilities: [
                    {
                        name: "[PC preview] unknown name",
                        forms: [
                            {
                                name: "[PC preview] unknown name",
                                type: "[PC preview] unknown type",
                                updateEnabled: "[PC preview] unknown updateEnabled",
                                scheduledUpdateTime: "[PC preview] unknown scheduledUpdateTime",
                                updateDuration: "[PC preview] unknown updateDuration",
                                supportDimensions: ["[PC preview] unknown supportDimensions"],
                                defaultDimension: "[PC preview] unknown defaultDimension",
                            }
                        ],
                    }
                ]
            }
        ]
    }
};

export const DispatchInfoMock = {
    verison: "[PC preview] unknown verison",
    dispatchAPI: "[PC preview] unknown dispatchAPI",
}